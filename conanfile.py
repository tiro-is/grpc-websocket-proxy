import os
from conans import ConanFile, tools


def get_version():
    git = tools.Git()
    try:
        return git.run("describe --always --dirty")
    except:
        return None


class GrpcwebsocketproxyConan(ConanFile):
    name = "grpc-websocket-proxy"
    version = get_version()
    go_full_name = "gitlab.com/tiro-is/grpc-websocket-proxy"
    license = """Copyright (C) 2016 Travis Cline

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
    source_url = "git@gitlab.com:tiro-is/grpc-websocket-proxy.git"
    description = """Wrap your grpc-gateway mux with this helper to expose
    streaming endpoints over websockets (Golang library)."""
    exports_sources = "wsproxy*", "examples*"

    build_requires = (
        "go_installer/[>=1.15]@tiro/stable",
    )

    def _go_get(self, package):
        self.output.warn(self.build_folder)
        with tools.environment_append({"GOPATH": "{}/builddeps".format(
                self.build_folder)}):
            self.run("go get -v {}".format(package))

    def build(self):
        # Package foreign dependecies with this one.
        with tools.chdir("wsproxy"):
            self._go_get("-d .")

    def package(self):
        # self.copy("*", dst="src", src="builddeps/src", keep_path=True)
        self.copy("*.go",
                  dst="src/{}".format(self.go_full_name),
                  src=".",
                  excludes=("builddeps/*",),
                  keep_path=True)

    def package_id(self):
        self.info.header_only()
